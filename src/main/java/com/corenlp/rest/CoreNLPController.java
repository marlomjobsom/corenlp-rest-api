package com.corenlp.rest;

import com.corenlp.core.CoreNLP;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * It is the REST API entry-point to the CoreNLP functionalities
 *
 * @author Marlom Oliveira
 */
@RestController
public class CoreNLPController {

    private final CoreNLP coreNLP;

    /**
     * Instantiates a new {@link CoreNLPController}.
     *
     * @param coreNLP the {@link CoreNLPController} instance
     */
    @Autowired
    public CoreNLPController(CoreNLP coreNLP) {
        this.coreNLP = coreNLP;
    }

    /**
     * It tokenizes a given text
     *
     * @param text the text to be tokenized
     * @return the text tokens
     */
    @PostMapping(path = "/tokenize", consumes = MimeTypeUtils.TEXT_PLAIN_VALUE)
    public List<String> tokenize(@RequestBody String text) {
        return this.coreNLP.tokenize(text);
    }
}
