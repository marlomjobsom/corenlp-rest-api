package com.corenlp.core;

import edu.stanford.nlp.simple.Document;
import edu.stanford.nlp.simple.Sentence;
import edu.stanford.nlp.simple.Token;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * It is the entry-point to the CoreNLP functionalities
 *
 * @author Marlom Oliveira
 */
@Service
public class CoreNLP {

    /**
     * It tokenizes a given text
     *
     * @param text the text to be tokenized
     * @return the text tokens
     */
    public List<String> tokenize(String text) {
        Document document = new Document(text);
        List<String> tokens = new ArrayList<>();

        for (Sentence sentence : document.sentences()) {
            for (Token token : sentence.tokens()) {
                tokens.add(token.word());
            }
        }

        return tokens;
    }
}
