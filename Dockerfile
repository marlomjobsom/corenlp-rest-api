# Build stage
FROM openjdk:11.0.10-jdk-slim-buster as build-stage
COPY . /core-nlp-rest-api
WORKDIR /core-nlp-rest-api
RUN ./gradlew bootWar

# Run stage
FROM openjdk:11.0.10-jre-slim-buster
COPY --from=build-stage /core-nlp-rest-api/build/libs/core-nlp-rest-api-1.0.war /core-nlp-rest-api-1.0.war
ENV PORT 8080
EXPOSE $PORT
ENTRYPOINT java -jar /core-nlp-rest-api-1.0.war --port=$PORT
