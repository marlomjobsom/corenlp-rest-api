# CoreNLP Service
It is a REST application that wraps CoreNLP functionalities.

# Requirements
* JDK 11
* Docker
* `gcloud`
* `kubectl`
* `minikube`
* VirtualBox

# Local

## Run through `.war` file

```shell
./gradlew bootWar
java -jar build/libs/core-nlp-rest-api-1.0.war --port=8080

# Test the application
curl -X POST -d "Hello world" -H "Content-Type: text/plain"  http://localhost:8080/tokenize/
```

## Run through `docker`

```shell
docker build -t core-nlp-rest-api:dev .
docker run -it --rm -d --name core-nlp-rest-api -p 8080:8080 core-nlp-rest-api:dev

# Test the application
curl -X POST -d "Hello world" -H "Content-Type: text/plain"  http://localhost:8080/tokenize/
```

## Run through `minikube`

### Start `minikube`

```shell
minikube start

# Optional: starts Kubernetes Dashboard to follow-up the `minikube`
minikube dashboard
```

### Build

```shell
minikube docker-env
eval $(minikube -p minikube docker-env)
docker build -t core-nlp-rest-api:dev .
```

### Deploy

```shell
kubectl create deployment core-nlp-rest-api --image=core-nlp-rest-api:dev
kubectl expose deployment core-nlp-rest-api --type=LoadBalancer --port=8080
minikube service core-nlp-rest-api
export URL=$(minikube service --url core-nlp-rest-api)

# Test the application
curl -X POST -d "Hello world" -H "Content-Type: text/plain"  $URL/tokenize/
```

### Destroy

```shell
kubectl delete deployment core-nlp-rest-api
kubectl delete service core-nlp-rest-api

# Optional: shut down and remove the `minikube` VM
minikube stop
minikube delete
```

# Google Cloud

## Cluster

Ensure the Kubernetes Engine is enabled by opening it for the first time.

```shell
gcloud container clusters create core-nlp-rest-api-e2-micro --zone us-central1-a --machine-type e2-micro
gcloud container clusters list

# Update `kubectl` context in case the current context be not pointing to the cluster created
kubectl config current-context
kubectl config get-contexts
kubectl config use-context [CONTEXT_NAME]

# Ensure connection with the cluster created
gcloud container clusters get-credentials core-nlp-rest-api-e2-micro --zone us-central1-a
```

## Static IP

The `corenlp-rest-api-static-ip` created here is referenced in the `Ingress`.

```shell
gcloud compute addresses create corenlp-rest-api-static-ip --global
gcloud compute addresses describe corenlp-rest-api-static-ip --global
```

## Domain

// TODO

## DNS

// TODO

## Build

```shell
export GCR_PREFIX=gcr.io/$(gcloud config get-value project -q)
docker build -t $GCR_PREFIX/core-nlp-rest-api:1.0 .
gcloud auth configure-docker --quiet
docker push $GCR_PREFIX/core-nlp-rest-api:1.0
gcloud container images list
```

## Deploy

```shell
kubectl create -f kubernetes.yaml
export IP=$(kubectl get service core-nlp-rest-api -o jsonpath='{.status.loadBalancer.ingress[0].ip}') 
export PORT=$(kubectl get service core-nlp-rest-api -o jsonpath='{.spec.ports[0].port}')
export URL=http://$IP:$PORT/ 

# Test the application
curl -X POST -d "Hello world" -H "Content-Type: text/plain"  $URL/tokenize/
```

## Destroy

```shell
kubectl delete -f kubernetes.yaml

# Optional: delete the cluster
gcloud container clusters list
gcloud container clusters delete core-nlp-rest-api-e2-micro --zone us-central1-a
```

# Usage

## `/tokenize/`
```shell
curl -X POST -d "Hello world" -H "Content-Type: text/plain"  $URL/tokenize/
```

# References
* [Machine Type Billing](https://cloud.google.com/compute/docs/machine-types#billing)
* [VM instances pricing](https://cloud.google.com/compute/vm-instance-pricing#e2_sharedcore_machine_types)